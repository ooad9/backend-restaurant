import { IsNotEmpty } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  customerPeople: number;

  @IsNotEmpty()
  customerName: string;

  @IsNotEmpty()
  customerPhone: number;
}
