import { Column, PrimaryGeneratedColumn } from 'typeorm';

export class Customer {
  @PrimaryGeneratedColumn()
  customerid: number;

  @Column()
  customerPeople: number;

  @Column()
  customerName: string;

  @Column()
  customerPhone: number;
}
