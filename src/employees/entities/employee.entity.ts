import { Transform } from 'class-transformer';
import { TransformFnParams } from 'class-transformer/types/interfaces';
import { Order } from 'src/orders/entities/order.entity';
import { Salary } from 'src/salarys/entities/salary.entity';
import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  firstname: string;

  @Column({
    length: '32',
  })
  lastname: string;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column()
  age: number;

  @Column()
  phone: string;

  @Column()
  position: string;

  // @OneToMany(() => Order, (order) => order.employee)
  // orders: Order[];

  // @OneToMany(() => Salary, (salary) => salary.employee)
  // salary: Salary[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteddAt: Date;

  foodQueue: any;
}
