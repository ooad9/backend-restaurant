import { IsNotEmpty, Length, Matches } from 'class-validator';
export class CreateEmployeeDto {
  @IsNotEmpty()
  @Length(1, 32)
  firstname: string;

  @IsNotEmpty()
  @Length(1, 32)
  lastname: string;

  @IsNotEmpty()
  @Length(4, 32)
  username: string;

  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  @IsNotEmpty()
  @Length(8, 32)
  password: string;

  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  phone: string;

  @IsNotEmpty()
  position: string;
}
