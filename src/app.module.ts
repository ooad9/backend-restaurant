import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SalarysModule } from './salarys/salarys.module';
import { Salary } from './salarys/entities/salary.entity';
import { TablesModule } from './tables/tables.module';
import { OrdersModule } from './orders/orders.module';
import { CustomersModule } from './customers/customers.module';
import { Order } from './orders/entities/order.entity';
import { DataSource } from 'typeorm';
import { Customer } from './customers/entities/customer.entity';
import { EmployeesModule } from './employees/employees.module';
import { FoodsModule } from './foods/foods.module';
import { FoodQueueModule } from './food-queue/food-queue.module';
import { Food } from './foods/entities/food.entity';
import { OrderItem } from './orders/entities/order-item';
import { Employee } from './employees/entities/employee.entity';
import { FoodQueue } from './food-queue/entities/food-queue.entity';
import { Table } from './tables/entities/table.entity';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(
      {
        type: 'mysql',
        host: 'db4free.net',
        port: 3306,
        username: 'monster_db',
        password: 'monster54321',
        database: 'monster_db',
        entities: [Food, Table, Order, OrderItem, Employee, Salary, FoodQueue],
        synchronize: true,
      },
      // {
      //   type: 'sqlite',
      //   database: 'monster-restaurant.sqlite',
      //   entities: [
      //     Table,
      //     Salary,
      //     Order,
      //     Customer,
      //     Food,
      //     FoodQueue,
      //     OrderItem,
      //     Employee,
      //   ],
      //   synchronize: true,
      // },
    ),
    SalarysModule,
    TablesModule,
    OrdersModule,
    CustomersModule,
    EmployeesModule,
    FoodsModule,
    FoodQueueModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
