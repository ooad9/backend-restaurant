import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { Table } from 'src/tables/entities/table.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Food } from 'src/foods/entities/food.entity';
import { OrderItem } from './entities/order-item';
import { FoodQueue } from 'src/food-queue/entities/food-queue.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Order,
      FoodQueue,
      OrderItem,
      Employee,
      Table,
      Food,
    ]),
  ],

  controllers: [OrdersController],
  providers: [OrdersService],
})
export class OrdersModule {}
