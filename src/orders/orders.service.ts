import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Food } from 'src/foods/entities/food.entity';
import { Table } from 'src/tables/entities/table.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(Food)
    private foodsRepository: Repository<Food>,
    @InjectRepository(Table)
    private tablesRepository: Repository<Table>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    const order: Order = new Order();
    order.table_number = createOrderDto.tableId;
    order.table = await this.tablesRepository.findOneBy({
      id: createOrderDto.tableId,
    });
    await this.ordersRepository.save(order);

    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.qty = od.qty;
      orderItem.food = await this.foodsRepository.findOneBy({
        id: od.foodId,
      });
      orderItem.name = orderItem.food.name;
      orderItem.order = order;
      await this.orderItemsRepository.save(orderItem);
    }
    await this.ordersRepository.save(order);
    return this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['orderItems.food', 'table'], // want to get food.price from orderItems for calculate total price
    });
  }

  async findOne(id: number) {
    const order = await this.ordersRepository.findOne({
      where: { id: id },
      relations: ['orderItems', 'orderItems.food', 'table'],
    });
    if (!order) {
      throw new NotFoundException();
    }
    return order;
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    const order = await this.ordersRepository.findOne({
      where: { id: id },
      relations: ['orderItems', 'orderItems.food', 'table'],
    });
    if (!order) {
      throw new NotFoundException();
    }
    if (!!updateOrderDto.tableId) {
      order.table_number = updateOrderDto.tableId;
      order.table.id = updateOrderDto.tableId;
    }
    const updatedOrder = { ...order, ...updateOrderDto };
    await this.ordersRepository.save(updatedOrder);
    return this.ordersRepository.save(updatedOrder);
  }

  async remove(id: number) {
    const order = await this.ordersRepository.findBy({ id: id });
    if (!order) {
      throw new NotFoundException();
    }
    return this.ordersRepository.softRemove(order);
  }
}
