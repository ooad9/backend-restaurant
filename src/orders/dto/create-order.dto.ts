import { IsNotEmpty } from 'class-validator';

export class CreateOrderDto {
  @IsNotEmpty()
  tableId: number;

  orderItems: CreateOrderItemDto[];
}
class CreateOrderItemDto {
  // id: number;
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  foodId: number;

  @IsNotEmpty()
  qty: number;

  // status: string;
}
