import { Employee } from 'src/employees/entities/employee.entity';
import { Table } from 'src/tables/entities/table.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import { OrderItem } from './order-item';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  table_number: number;

  @ManyToOne(() => Table, (table) => table.orders)
  table: Table;

  // @Column()
  // qty: number;

  @CreateDateColumn()
  timestampOpen: Date;

  @UpdateDateColumn()
  timestampClose: Date;

  @DeleteDateColumn()
  deleteddAt: Date;

  // @Column({ type: 'float' })
  // total: number;

  // @Column()
  // status: string;

  // @ManyToOne(() => Employee, (employee) => employee.orders)
  // employee: Employee;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];
}
