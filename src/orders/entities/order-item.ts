import { FoodQueue } from 'src/food-queue/entities/food-queue.entity';
import { Food } from 'src/foods/entities/food.entity';
import { Table } from 'src/tables/entities/table.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Order } from './order.entity';

@Entity()
export class OrderItem {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Order, (orders) => orders.orderItems)
  order: Order;

  @ManyToOne(() => Food, (food) => food.orderItems)
  food: Food;

  @Column()
  name: string;

  // @Column({ type: 'float' })
  // price: number;

  @Column()
  qty: number;

  // @ManyToOne(() => Order, (orders) => orders.table)
  // table: Table;

  // @OneToMany(() => FoodQueue, (foodQueue) => foodQueue.orderItem)
  // foodQueues: FoodQueue[];

  // @Column()
  // status: string;

  @CreateDateColumn()
  timestampOpen: Date;

  @UpdateDateColumn()
  timestampClose: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}
