import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { FoodQueueService } from './food-queue.service';
import { CreateFoodQueueDto } from './dto/create-food-queue.dto';
import { UpdateFoodQueueDto } from './dto/update-food-queue.dto';

@Controller('food-queue')
export class FoodQueueController {
  constructor(private readonly foodQueueService: FoodQueueService) {}

  @Post()
  create(@Body() createFoodQueueDto: CreateFoodQueueDto) {
    return this.foodQueueService.create(createFoodQueueDto);
  }

  @Get()
  findAll() {
    return this.foodQueueService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.foodQueueService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateFoodQueueDto: UpdateFoodQueueDto,
  ) {
    return this.foodQueueService.update(+id, updateFoodQueueDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.foodQueueService.remove(+id);
  }
}
