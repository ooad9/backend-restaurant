import { Injectable } from '@nestjs/common';
import { CreateFoodQueueDto } from './dto/create-food-queue.dto';
import { UpdateFoodQueueDto } from './dto/update-food-queue.dto';

@Injectable()
export class FoodQueueService {
  create(createFoodQueueDto: CreateFoodQueueDto) {
    return 'This action adds a new foodQueue';
  }

  findAll() {
    return `This action returns all foodQueue`;
  }

  findOne(id: number) {
    return `This action returns a #${id} foodQueue`;
  }

  update(id: number, updateFoodQueueDto: UpdateFoodQueueDto) {
    return `This action updates a #${id} foodQueue`;
  }

  remove(id: number) {
    return `This action removes a #${id} foodQueue`;
  }
}
