import { Employee } from 'src/employees/entities/employee.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class FoodQueue {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 64 })
  name: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteddAt: Date;

  @Column()
  status: string;
  // @ManyToOne(() => Employee, (employee) => employee.foodQueue)
  // employee: Employee;

  // @ManyToOne(() => OrderItem, (orderItem) => orderItem.foodQueues)
  // orderItem: OrderItem;
}
