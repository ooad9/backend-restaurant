import { IsNotEmpty } from 'class-validator';

export class CreateFoodQueueDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  status: string;
}
