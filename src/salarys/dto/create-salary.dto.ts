import { IsNotEmpty } from 'class-validator';

export class CreateSalaryDto {
  id: number;

  @IsNotEmpty()
  date: string;

  @IsNotEmpty()
  payCircle: string;

  @IsNotEmpty()
  hours: number;

  @IsNotEmpty()
  total: number;
}
