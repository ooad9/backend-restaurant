import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Salary } from './entities/salary.entity';

@Injectable()
export class SalarysService {
  constructor(
    @InjectRepository(Salary)
    private salarysRepository: Repository<Salary>,
  ) {}

  create(createSalaryDto: CreateSalaryDto) {
    return this.salarysRepository.save(createSalaryDto);
  }

  findAll() {
    return this.salarysRepository.find({});
  }

  async findOne(id: number) {
    const salary = await this.salarysRepository.findOne({
      where: { id: id },
    });
    if (!salary) {
      throw new NotFoundException();
    }
    return salary;
  }

  async update(id: number, updateSalaryDto: UpdateSalaryDto) {
    const salary = await this.salarysRepository.findOneBy({ id: id });
    if (!salary) {
      throw new NotFoundException();
    }
    const updatedSalary = { ...salary, ...updateSalaryDto };
    return this.salarysRepository.save(updatedSalary);
  }

  async remove(id: number) {
    const salary = await this.salarysRepository.findOneBy({ id: id });
    if (!salary) {
      throw new NotFoundException();
    }
    return this.salarysRepository.softRemove(salary);
  }
}
