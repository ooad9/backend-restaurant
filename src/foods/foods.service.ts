import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateFoodDto } from './dto/create-food.dto';
import { UpdateFoodDto } from './dto/update-food.dto';
import { Food } from './entities/food.entity';

@Injectable()
export class FoodsService {
  constructor(
    @InjectRepository(Food)
    private foodsRepository: Repository<Food>,
  ) {}

  create(createFoodDto: CreateFoodDto) {
    return this.foodsRepository.save(createFoodDto);
  }

  findAll() {
    return this.foodsRepository.find();
  }

  findOne(id: number) {
    return this.foodsRepository.findOne({ where: { id } });
  }

  update(id: number, updateFoodDto: UpdateFoodDto) {
    return `This action updates a #${id} food`;
  }

  async remove(id: number) {
    const food = await this.foodsRepository.findOneBy({ id });
    if (!food) {
      throw new NotFoundException();
    }
    return this.foodsRepository.softRemove(food);
  }
}
