import { IsNotEmpty, Length } from 'class-validator';

export class CreateFoodDto {
  @IsNotEmpty()
  @Length(1, 32)
  name: string;

  @IsNotEmpty()
  type: string;

  img = 'no-img.png';

  @IsNotEmpty()
  price: number;
}
